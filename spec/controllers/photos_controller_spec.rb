require 'rails_helper'

RSpec.describe PhotosController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    let(:photo) { Photo.create( title: "Photo Title",
                                image_file_name: "photo.jpg",
                                image_file_size: 10,
                                image_content_type: "image/jpeg"
                )}

    before do
      photo
    end

    it "returns http success" do
      get :show, params: { id: photo.id }
      expect(response).to have_http_status(:success)
    end
  end

end
