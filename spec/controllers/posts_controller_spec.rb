require 'rails_helper'

RSpec.describe PostsController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    let(:post) { Post.create(title: "Post title", body: "Post body") }
    
    before do
      post
    end

    it "returns http success" do
      get :show, params:  { id: post.id }
      expect(response).to have_http_status(:success)
      post.destroy
    end
  end
end
