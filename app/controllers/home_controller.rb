class HomeController < ApplicationController
  include HomeHelper

  def index
    @photos = Photo.all
  end

  def about
  end

  def contact
  end

  def contact_email
    if validate_captcha(params["g-recaptcha-response"])
      ContactMailer.contact_email(params).deliver
      flash[:notice] = "You email has been successfully sent."
      redirect_to action: "contact"
    else
      flash[:notice] = "Something went wrong. Please try again."
      render "contact"
    end
  end
end
