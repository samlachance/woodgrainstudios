class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@woodgrainstudios.com'
  layout 'mailer'
end
