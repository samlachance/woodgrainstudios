class ContactMailer < ApplicationMailer
  def contact_email(params)
    @params = params

    mail(to: 'woodgrainstudios@bellsouth.net', subject: "Woodgrain email from: #{@params[:email]}")
  end
end
