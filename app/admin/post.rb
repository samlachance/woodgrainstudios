ActiveAdmin.register Post do
  permit_params :title, :body

  form do |f|
    f.semantic_errors
    
    inputs "Details" do
      f.input :title
      f.input :body, :as => :ckeditor
    end
    f.actions
  end

  show do 
    post.body.html_safe
  end
end
