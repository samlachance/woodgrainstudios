ActiveAdmin.register Photo do
  permit_params :title, :image, :image_file_name, :image_content_type, :image_file_size

  form do |f|
    inputs "Preview" do
      img(src: photo.image.url)
    end

    inputs "Details" do
      f.input :title
      f.input :image, as: :file
    end

    f.actions
  end

  show do
    h3 photo.created_at
    img(src: photo.image.url, class: "admin-image")
  end
end
