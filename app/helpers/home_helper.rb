module HomeHelper
  require "uri"
  require "net/http"

  def validate_captcha(response)
    res = Net::HTTP.post_form(
    URI.parse('https://www.google.com/recaptcha/api/siteverify'),
      {
        'secret' => ENV["WOODGRAIN_CAPTCHA_SECRET"],
        'remoteip'   => request.ip,
        'response'   => response
      }
    )
    body = JSON.parse(res.body)

    body["success"]
  end
end
