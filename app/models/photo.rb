class Photo < ApplicationRecord
  has_attached_file :image,
    :styles => {
      :medium => {
        :geometry => '1000x1000',
        :quality => 90
      }
    }
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
