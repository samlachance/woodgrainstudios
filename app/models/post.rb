class Post < ApplicationRecord
  validates_presence_of :title, :body
  validates_length_of :title, minimum: 8
  validates_uniqueness_of :title
end
